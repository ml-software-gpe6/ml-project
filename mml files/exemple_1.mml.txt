%load est un
load 'monoprix.csv' as data


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%define predictive variable
% on
data.set_var_col("y"->3)

%%% define the graph structure %%%%
new_classifier model

model.add("clf"->"cnn")

model.add("metric"->"accuracy")



%% evaluation in {cross_val, train_test_split}
model.add("evaluation"->"cross_val")

model.add("k-fold"->5 )
%%%% strure defined %%%%

model.launch("data"->data)


show model.graph
show model.summary
