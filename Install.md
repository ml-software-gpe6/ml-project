# Installation

## Requirements
### For Windows users
- Install [Ecplise IDE for Java and DSL developers](https://ftp.acc.umu.se/mirror/eclipse.org/technology/epp/downloads/release/2019-12/R/eclipse-dsl-2019-12-R-win32-x86_64.zip)
- Install [Python](https://www.python.org/ftp/python/3.8.2/python-3.8.2-amd64.exe)
- Install [R](https://pbil.univ-lyon1.fr/CRAN/bin/windows/base/R-3.6.3-win.exe) and save the path in which it is installed (in our case it's [C:\Program Files\R-3.6.3\bin]() so you should replace it by your path in the following command lines)
- Now go to cmd in the start menu and type each of the following command lines: 
```bash
pip install scipy
```
```bash
pip install numpy
```
```bash
pip install -U scikit-learn
```
```bash
cd C:\Program Files\R-3.6.3\bin
```

```bash
C:\Program Files\R-3.6.3\bin>RScript.exe  -e "install.packages('e1071', repos
='http://cran.rstudio.com/')"
``````
```bash
C:\Program Files\R-3.6.3\bin>RScript.exe  -e "install.packages('caret', repos
='http://cran.rstudio.com/')"
```
```bash
C:\Program Files\R-3.6.3\bin>RScript.exe  -e "install.packages('randomForest', repos
='http://cran.rstudio.com/')"
```
```bash
C:\Program Files\R-3.6.3\bin>RScript.exe  -e "install.packages('glmnet', repos
='http://cran.rstudio.com/')"
```
```bash
C:\Program Files\R-3.6.3\bin>RScript.exe  -e "install.packages('rpart', repos
='http://cran.rstudio.com/')"
```
```bash
C:\Program Files\R-3.6.3\bin>RScript.exe  -e "install.packages('MLmetrics', repos
='http://cran.rstudio.com/')"
```


### For Linux users
- Install [Ecplise IDE for Java and DSL developers](http://ftp.fau.de/eclipse/technology/epp/downloads/release/2019-12/R/eclipse-dsl-2019-12-R-linux-gtk-x86_64.tar.gz)
- Install R by typing in terminal :

```bash
sudo add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran35/"
```
```bash
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
```
```bash
sudo apt update
sudo apt install r-base
```





- Install Python by typing 
```bash
sudo apt update
sudo apt install software-properties-common
```
```bash
sudo add-apt-repository ppa:deadsnakes/ppa
```
```bash
sudo apt install python3.7
```

- Install dependencies by typing:
```bash
sudo apt-get install python-sklearn
```
```bash
R
``````
```bash
install.packages("e1071")
```
```bash
install.packages("caret")
```
```bash
install.packages("randomForest")
```
```bash
install.packages("glmnet")
```
```bash
install.packages("rpart")
```
```bash
install.packages("MLmetrics", repos = "https://cran.rstudio.com")
```
## For Mac users
- Install [Ecplise IDE for Java and DSL developers](http://ftp.fau.de/eclipse/technology/epp/downloads/release/2019-12/R/eclipse-dsl-2019-12-R-macosx-cocoa-x86_64.dmg)
- Install [R](https://pbil.univ-lyon1.fr/CRAN/bin/macosx/R-3.6.3.pkg)
- Install [Python](https://www.python.org/ftp/python/3.7.7/python-3.7.7-macosx10.9.pkg)
- Install dependencies by typing into command prompt:
```bash
pip install -U numpy scipy scikit-learn
```
```bash
Rscript -e "install.packages('e1071')"
```
```bash
Rscript -e "install.packages('caret')"
```
```bash
Rscript -e "install.packages('randomForest')"
```
```bash
Rscript -e "install.packages('glmnet')"
```
```bash
Rscript -e "install.packages('rpart')"
```
```bash
Rscript -e "install.packages('MLmetrics', repos = 'https://cran.rstudio.com')"
```


## App installation
In order to install the application, we need to type into command prompt:
```bash
cd (The path to the folder where you want to download the app)
```
```bash
git clone https://gitlab.com/ml-software-gpe6/ml-project/-/tree/master
```
Now open eclipse, select 'Import existing projects', check the 'Select root directory' box and browse into the 'ml-project\server\mml-webapp', click on 'OK' and then click on the 'Finish' button.

Now you should see on the left side of the screen a folder called 'mml-webapp'.

You should click on it and then on the src/main/java, passing through 'default package' and then you right click on 'main.java' file and you choose 'run as java application'.

From there you go to your browser and open this address [localhost:8080](localhost:8080)\

At this stage, a website should show-up.

If so, congratulations, this means that you have successfully installed the application and you are ready to use it.




