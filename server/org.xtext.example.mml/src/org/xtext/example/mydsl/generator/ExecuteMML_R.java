package org.xtext.example.mydsl.generator;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.xtext.example.mydsl.mml.CSVParsingConfiguration;
import org.xtext.example.mydsl.mml.DT;
import org.xtext.example.mydsl.mml.DataInput;
import org.xtext.example.mydsl.mml.FrameworkLang;
import org.xtext.example.mydsl.mml.LogisticRegression;
import org.xtext.example.mydsl.mml.MLAlgorithm;
import org.xtext.example.mydsl.mml.MLChoiceAlgorithm;
import org.xtext.example.mydsl.mml.MMLModel;
import org.xtext.example.mydsl.mml.RandomForest;
import org.xtext.example.mydsl.mml.SVM;
import org.xtext.example.mydsl.mml.SVMClassification;
import org.xtext.example.mydsl.mml.SVMKernel;
import org.xtext.example.mydsl.mml.StratificationMethod;
import org.xtext.example.mydsl.mml.Validation;
import org.xtext.example.mydsl.mml.ValidationMetric;

import com.google.common.io.Files;

public class ExecuteMML_R {

	
	
	private String mkValueInSingleQuote(String val) {
		return "'" + val + "'";
	}
	
	public String executeMML(MMLModel result, FrameworkLang framework, MLChoiceAlgorithm algo) {
		
		//Import all functions in tools using tool object.
		RMMlExecutorTools tool = new RMMlExecutorTools();
			
		DataInput dataInput = result.getInput();
		String fileLocation = dataInput.getFilelocation();
	
		
		//String pythonImport = "import pandas as pd\n"; 
		String DEFAULT_COLUMN_SEPARATOR = ","; // by default
		String csv_separator = DEFAULT_COLUMN_SEPARATOR;
		CSVParsingConfiguration parsingInstruction = dataInput.getParsingInstruction();
		if (parsingInstruction != null) {			
			System.err.println("parsing instruction..." + parsingInstruction);
			csv_separator = parsingInstruction.getSep().toString();
		}
		
		
		//String pandasCode = "install.packages(\"datasets\")\n";
		String pandasCode = "library(datasets)\n";
		//pandasCode += "install.packages(\"e1071\")\n";
		pandasCode += "library(e1071)\n";
		//pandasCode += "install.packages(\"caret\")\n";
		pandasCode += "library(caret)\n";
		//pandasCode += "install.packages(\"ML\")\n";
		//pandasCode += "library(ML)\n";
		//pandasCode += "install.packages(\"MLmetrics\")\n";
		//pandasCode += "library(MLmetrics)\n";
		
		
		String csvReading = "mml_data <- read.csv(" + mkValueInSingleQuote(fileLocation) + ", sep=" + mkValueInSingleQuote(csv_separator) + ")\n";						
		pandasCode += csvReading;
		
		// Ajout des autres proprités de l'algo
		Validation validation = result.getValidation();
		StratificationMethod stratMethod = validation.getStratification();
		List<ValidationMetric> metrics = validation.getMetric();
		List<MLChoiceAlgorithm> list_algos = new ArrayList<MLChoiceAlgorithm>();
		
			MLAlgorithm mlalgo = algo.getAlgorithm();
			//FrameworkLang framework = algo.getFramework();
			
			
			if(framework == FrameworkLang.R) {
				//System.out.println("R is targeted as framework");
				
				pandasCode += "\ncat('R is the framework selected', '\\n')\n";
				
				list_algos.add(algo);
				
				if(mlalgo instanceof DT) {
					pandasCode += "\ncat('DecisionTree', '\n')\n";
					// faire du casting
					DT dt = (DT) mlalgo;
					int dtMaxDepth = dt.getMax_depth();
					//System.out.println(dtMaxDepth);
					
					//pandasCode += "install.packages(\"rpart\")\n";
					String topCode = pandasCode + "library(rpart)\n";
					
										
					if(dtMaxDepth>0) {
						String modelCode = "dt <- rpart(y ~ ., data=train, control = rpart.control(maxdepth="+dtMaxDepth+"))\n";
						pandasCode = tool.generateModel("dt", stratMethod, metrics, topCode, modelCode);
					}
					else {
						String modelCode = "dt <- rpart(y ~ ., data=train)\n";
						pandasCode = tool.generateModel("dt", stratMethod, metrics, topCode, modelCode);
					}

					//System.out.println(pandasCode);
					
				}
				
				else if(mlalgo instanceof SVM) {
					pandasCode += "\ncat('SVM', '\\n')\n";
					// faire du casting
					SVM svm = (SVM) mlalgo;
					
					//les paramètres
					String C         = svm.getC();
					String gamma     = svm.getGamma();
					SVMKernel kernel = svm.getKernel();
					
					System.out.println(C);
					System.out.println(gamma);
					System.out.println("the kernel is " + kernel);
					
					SVMClassification svmClass = svm.getSvmclassification(); //type de classification
					
					//pandasCode = "from sklearn import svm\n" + pandasCode;
					
					//test des paramètres
							
					// 1er paramètre doit être le 1er à tester car c'est le type de classification à utiliser (il y en a 3)
					
					if(svmClass == SVMClassification.CCLASS || svmClass == null) { //SVC
						//pandasCode += "from sklearn.svm import SVC\n";
						String modelCode = tool.getSVMOutput("'C-classification'", C, gamma, kernel, "");
						pandasCode = tool.generateModel("clf", stratMethod, metrics, pandasCode, modelCode);
					}
					
					else if(svmClass == SVMClassification.NU_CLASS) { //nuSVC
						//pandasCode += "from sklearn.svm import NuSVC\n";
						String modelCode = tool.getSVMOutput("'nu-classification'", C, gamma, kernel, "");
						pandasCode = tool.generateModel("clf", stratMethod, metrics, pandasCode, modelCode);
					}
					
					else if(svmClass == SVMClassification.ONE_CLASS) { //LinearSVC
						//pandasCode += "from sklearn.svm import OneClassSVM\n";
						// !!! not for iris (multiclass)
						String modelCode = tool.getSVMOutput("'one-classification'", C, gamma, kernel, "");
						pandasCode = tool.generateModel("clf", stratMethod, metrics, pandasCode, modelCode);
					}
					
					
					//pandasCode+= "clf = svm.SVC(C = "+ C + ")";
					
					
					
					//if(C == null || Integer.parseInt(C)<0) {
						//C
					//}
					//pandasCode = generateModel("clf", stratMethod, metrics, pandasCode);
					//System.out.println(pandasCode);
					
				}
				
				else if(mlalgo instanceof RandomForest) {
					pandasCode += "\ncat('RandomForest', '\\n')\n";
					// faire du casting
					//RandomForest rf = (RandomForest) mlalgo;
					//pandasCode += "install.packages(\"randomForest\")\n";
					String topCode = pandasCode + "library(randomForest)\n";
					String modelCode = "rf <- randomForest(y ~ ., data=train, importance=TRUE, proximity=TRUE, ntree=500)\n";
					pandasCode = tool.generateModel("rf", stratMethod, metrics, topCode, modelCode);
					//System.out.println(pandasCode);
				}
				
				else if(mlalgo instanceof LogisticRegression) {
					pandasCode += "\ncat('  LogisticRegression', '\\n')\n";
					String topCode = pandasCode + "library(glmnet)\n";
					String modelCode = "lr <- glmnet(x = as.matrix(X_train), y = y_train, family = 'multinomial',alpha = 1,lambda = 0.2)\n";
					pandasCode = tool.generateModel("lr", stratMethod, metrics, topCode, modelCode);
					//System.out.println(pandasCode);
				}
				
				
			}
			else {
				System.out.println("R -> UNSUPPORTED FRAMEWORK");
			}
			
		
		
		try {
			System.out.println("Voici le code \n" + pandasCode);
			Files.write(pandasCode.getBytes(), new File("mml.R"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// end of Python generation
		
		/*
		 * Calling generated R script (basic solution through systems call)
		 * we assume that "Rscript" is in the path
		 */
		
		try {
			Process p;
			p = Runtime.getRuntime().exec("Rscript mml.R");
			//BufferedReader in = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
			
			String line; 
			String line_tem = "";
			//boolean title = false;
			String html = "";
			int nbmetric = metrics.size();
				if((line = in.readLine()) != null) {
					line_tem = line;
					if(mlalgo instanceof DT) {
						//html += "<br/>R-Result of Decision Tree : <br/>";
						for(int i=0; i<nbmetric; i+=1) {
							if(line_tem == "") {
								line = in.readLine();
								html += line + "\n";
							}else {
								html += line_tem + "\n";
								line_tem = "";
							}
						}
					}else if(mlalgo instanceof RandomForest) {
						//html += "<br/>R-Result of Random Forest : <br/>";
						for(int i=0; i<nbmetric; i+=1) {
							if(line_tem == "") {
								line = in.readLine();
								html += line + "\n";
							}else {
								html += line_tem + "\n";
								line_tem = "";
							}
						}
					}else if(mlalgo instanceof SVM) {
						//html += "<br/>R-Result of SVM : <br/>";
						for(int i=0; i<nbmetric; i+=1) {
							if(line_tem == "") {
								line = in.readLine();
								html += line + "\n";
							}else {
								html += line_tem + "\n";
								line_tem = "";
							}
						}
					}else if(mlalgo instanceof LogisticRegression) {
						//html += "<br/>R-Result of Logistic Regression : <br/>";
						for(int i=0; i<nbmetric; i+=1) {
							if(line_tem == "") {
								line = in.readLine();
								html += line + "\n";
							}else {
								html += line_tem + "\n";
								line_tem = "";
							}
						}
					}
				}
				
			
			
			
			return html;
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return "error";
		
	}
	
}


