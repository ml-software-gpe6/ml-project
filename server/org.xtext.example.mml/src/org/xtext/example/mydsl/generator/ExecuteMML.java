package org.xtext.example.mydsl.generator;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import org.xtext.example.mydsl.mml.CSVParsingConfiguration;
import org.xtext.example.mydsl.mml.DT;
import org.xtext.example.mydsl.mml.DataInput;
import org.xtext.example.mydsl.mml.FrameworkLang;
import org.xtext.example.mydsl.mml.LogisticRegression;
import org.xtext.example.mydsl.mml.MLAlgorithm;
import org.xtext.example.mydsl.mml.MLChoiceAlgorithm;
import org.xtext.example.mydsl.mml.MMLModel;
import org.xtext.example.mydsl.mml.RandomForest;
import org.xtext.example.mydsl.mml.SVM;
import org.xtext.example.mydsl.mml.SVMClassification;
import org.xtext.example.mydsl.mml.SVMKernel;
import org.xtext.example.mydsl.mml.StratificationMethod;
import org.xtext.example.mydsl.mml.Validation;
import org.xtext.example.mydsl.mml.ValidationMetric;

import com.google.common.io.Files;

public class ExecuteMML {

	
	
	private String mkValueInSingleQuote(String val) {
		return "'" + val + "'";
	}
	
	
	public String executeMML(MMLModel result) {
		//Import all functions in tools using tool object.
		PythonMmlExecutorTools tool = new PythonMmlExecutorTools();
		
		String res_python = "", res_R = "";
			
		DataInput dataInput = result.getInput();
		String fileLocation = dataInput.getFilelocation();
	
		
		String pythonImport = "import pandas as pd\n"; 
		String DEFAULT_COLUMN_SEPARATOR = ","; // by default
		String csv_separator = DEFAULT_COLUMN_SEPARATOR;
		CSVParsingConfiguration parsingInstruction = dataInput.getParsingInstruction();
		if (parsingInstruction != null) {			
			System.err.println("parsing instruction..." + parsingInstruction);
			csv_separator = parsingInstruction.getSep().toString();
		}
		String csvReading = "mml_data = pd.read_csv(" + mkValueInSingleQuote(fileLocation) + ", sep=" + mkValueInSingleQuote(csv_separator) + ")\n";						
		String pandasCode = pythonImport + csvReading;
		
		
		// Ajout des autres proprités de l'algo
		List<MLChoiceAlgorithm> algos = result.getAlgorithms();
		Validation validation = result.getValidation();
		StratificationMethod stratMethod = validation.getStratification();
		List<ValidationMetric> metrics = validation.getMetric();
		
		for(MLChoiceAlgorithm algo : algos) {
			MLAlgorithm mlalgo = algo.getAlgorithm();
			FrameworkLang framework = algo.getFramework();
			
			
			if(framework == FrameworkLang.SCIKIT) {
				
				System.out.println("scikit-learn is targeted as framework");
				
				//Ajout d'un print pour identifier l eframework utilisé
				pandasCode += "print('SCIKIT-LEARN is the framework selected')\n";
				
				if(mlalgo instanceof DT) {
					pandasCode += "print('DecisionTree')\n";
					
					// faire du casting
					DT dt = (DT) mlalgo;
					int dtMaxDepth = dt.getMax_depth();
					//System.out.println(dtMaxDepth);
					
					pandasCode = "from sklearn import tree\n" + pandasCode;
					
					if(dtMaxDepth>0) {
						pandasCode+= "dt = tree.DecisionTreeClassifier(max_depth="+dtMaxDepth+")";
					}
					else {
						pandasCode+= "dt = tree.DecisionTreeClassifier()";
					}
					
					pandasCode = tool.generateModel("dt", stratMethod, metrics, pandasCode);
					
				}
				
				else if(mlalgo instanceof SVM) {
					
					pandasCode += "print('SVM')\n";
					// faire du casting
					SVM svm = (SVM) mlalgo;
					
					//les paramètres
					String C         = svm.getC();
					String gamma     = svm.getGamma();
					SVMKernel kernel = svm.getKernel();
					
					SVMClassification svmClass = svm.getSvmclassification(); //type de classification
					
					//pandasCode = "from sklearn import svm\n" + pandasCode;
					
					//test des paramètres
							
					// 1er paramètre doit être le 1er à tester car c'est le type de classification à utiliser (il y en a 3)
					
					if(svmClass == SVMClassification.CCLASS) { //SVC
						pandasCode = "from sklearn.svm import SVC\n" + pandasCode;
						pandasCode = tool.getSVMOutput("SVC", C, gamma, kernel, pandasCode);
					}
					
					else if(svmClass == SVMClassification.NU_CLASS) { //nuSVC
						pandasCode = "from sklearn.svm import NuSVC\n" + pandasCode;
						pandasCode = tool.getSVMOutput("NuSVC", C, gamma, kernel, pandasCode);
					}
					
					else if(svmClass == SVMClassification.ONE_CLASS) { //LinearSVC
						pandasCode = "from sklearn.svm import OneClassSVM\n" + pandasCode;
						pandasCode = tool.getSVMOutput("OneClassSVM", C, gamma, kernel, pandasCode);
					}
					
					pandasCode = tool.generateModel("clf", stratMethod, metrics, pandasCode);
					
				}
				
				else if(mlalgo instanceof RandomForest) {
					pandasCode += "print('RandomForest')\n";
					
					pandasCode = "from sklearn.ensemble import RandomForestClassifier \n" + pandasCode;
					pandasCode += "rf = RandomForestClassifier(n_estimators=50)";
					pandasCode = tool.generateModel("rf", stratMethod, metrics, pandasCode);
				}
				
				else if(mlalgo instanceof LogisticRegression) {
					pandasCode += "print('LogisticRegression')\n";
					pandasCode = "from sklearn.linear_model import LogisticRegression\n" + pandasCode;
					pandasCode += "lr = LogisticRegression()";
					pandasCode = tool.generateModel("lr", stratMethod, metrics, pandasCode);
				}
				
				try {
					//System.out.println("Voici le code \n" + pandasCode);
					Files.write(pandasCode.getBytes(), new File("mml.py"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				/*
				 * Calling generated Python script (basic solution through systems call)
				 * we assume that "python" is in the path
				 */
				
				try {
					Process p;
					p = Runtime.getRuntime().exec("python mml.py");
					//BufferedReader in = new BufferedReader(new InputStreamReader(p.getErrorStream()));
					BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
					
					String line; 
					String acc = "";
					while ((line = in.readLine()) != null) {
						acc += line + "\n";
						
					}
					res_python = acc;
					
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				
			}
			else if (framework == FrameworkLang.R) {
				
				ExecuteMML_R executeR = new ExecuteMML_R();
				
				// Chaque nouveau algorithme R écrase l'ancien algorithme dans le fichier mml.R
				res_R += executeR.executeMML(result, framework, algo) +"-".repeat(20) + "\n";
				
			}
			else {
				System.out.println("UNSUPPORTED FRAMEWORK");
			}
			
		}
		return res_python + res_R;
	
	}
	
}

