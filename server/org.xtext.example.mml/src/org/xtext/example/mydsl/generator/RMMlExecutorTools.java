package org.xtext.example.mydsl.generator;

import java.util.List;

import org.xtext.example.mydsl.mml.CrossValidation;
import org.xtext.example.mydsl.mml.SVMKernel;
import org.xtext.example.mydsl.mml.StratificationMethod;
import org.xtext.example.mydsl.mml.TrainingTest;
import org.xtext.example.mydsl.mml.ValidationMetric;

public class RMMlExecutorTools {
	public String[] getAccuracyOutput_cv(ValidationMetric metric, String printCode, String pandasCode, String res, String row_name) {
		
		String[] allStrings = new String[4];

		
		if(metric == ValidationMetric.BALANCED_ACCURACY) {
			pandasCode += "\tbalanced_accuracy = byClass$'Balanced Accuracy'\n";
			printCode += "cat('leBALANCED_ACCURACY : v=', row_mean[c('bal_acc_val_1','bal_acc_val_2','bal_acc_val_3')],'\n')\n";
			res += "balanced_accuracy";
			row_name += "'bal_acc_val_1','bal_acc_val_2','bal_acc_val_3'";
		}
		
		else if(metric == ValidationMetric.RECALL) {
			pandasCode += "\trecall = byClass$Recall\n";
			printCode += "cat('leRECALL : v=', row_mean[c('recall_val_1','recall_val_2','recall_val_3')],'\n')\n";
			res += "recall";
			row_name += "'recall_val_1','recall_val_2','recall_val_3'";
		}
		
		else if(metric == ValidationMetric.MACRO_RECALL) {
			pandasCode += "\tmacro_recall = mean(byClass$Recall)\n";
			printCode += "cat('MACRO_RECALL : v=', row_mean['macro_recall'],'\n')\n";
			res += "macro_recall";
			row_name += "'macro_recall'";
		}
		
		else if(metric == ValidationMetric.PRECISION) {
			pandasCode += "\tprecision = byClass$Precision\n";
			printCode += "cat('laPRECISION : v=', row_mean[c('precision_val_1','precision_val_2','precision_val_3')],'\n')\n";
			res += "precision";
			row_name += "'precision_val_1','precision_val_2','precision_val_3'";
		}
		
		else if(metric == ValidationMetric.MACRO_PRECISION) {
			pandasCode += "\tmacro_precision = mean(byClass$Precision)\n";
			printCode += "cat('leMACRO_PRECISION : v=', row_mean['macro_precision'],'\n')\n";
			res += "macro_precision";
			row_name += "'macro_precision'";
		}
		
		else if(metric == ValidationMetric.F1 ) {
			pandasCode += "\tF1 = byClass$F1\n";
			printCode += "cat('  F1 : v=', row_mean[c('F1_val_1','F1_val_2','F1_val_3')],'\n')\n";
			res += "F1";
			row_name += "'F1_val_1','F1_val_2','F1_val_3'";
		}
		else if(metric == ValidationMetric.MACRO_F1) {
			pandasCode += "\tmacro_F1 = mean(byClass$F1)\n";
			printCode += "cat('  MACRO_F1 : v=', row_mean['macro_F1'],'\n')\n";
			res += "macro_F1";
			row_name += "'macro_F1'";
		}
		
		else if(metric == ValidationMetric.ACCURACY) {
			pandasCode += "\taccuracy = overall['Accuracy',]\n";
			printCode += "cat('  ACCURACY : v=', row_mean['accuracy'],'\n')\n";
			res += "accuracy";
			row_name += "'accuracy'";
		}
		
		/*else if(metric == ValidationMetric.MACRO_ACCURACY) {
			topImports += "accuracy_score";
			pandasCode += "\nprint('MACRO_ACCURACY : ', accuracy_score(y_test, y_pred, average='macro'))";
		}*/
		
		allStrings[0] = printCode;
		allStrings[1] = pandasCode;
		allStrings[2] = res;
		allStrings[3] = row_name;
		
		return allStrings;
	}
	
public String[] generateValidationMetricOutput_cv(List<ValidationMetric> metrics, String pandasCode) {
		
		//Importation in the top of python
		String printCode = "";
		String res = "";
		String row_name = "";
		
		ValidationMetric firstMetric = metrics.get(0);
		
		
		String[] allStrings = new String[4];
		
		//pandasCode += "\tCM <-confusionMatrix(y_test,y_pred)\n";
		//pandasCode += "\tbyClass<-as.data.frame(CM$byClass)\n";
		//pandasCode += "\toverall<-as.data.frame(CM$overall)\n";
		
		allStrings = getAccuracyOutput_cv(firstMetric, printCode, pandasCode, res, row_name);
		printCode = allStrings[0];
		pandasCode = allStrings[1];
		res = allStrings[2];
		row_name = allStrings[3];
		
		System.out.println(metrics.size());
		for(int i=1; i<metrics.size();i++) {
			
			res += ", "; //Ajout d'un nouveau dans return
			row_name += ", ";
			
			ValidationMetric metric = metrics.get(i);
			String[] _allStrings = new String[4];
			
			_allStrings = getAccuracyOutput_cv(metric, printCode, pandasCode, res, row_name);
			
			printCode = _allStrings[0];
			pandasCode = _allStrings[1];
			res = _allStrings[2];
			row_name = _allStrings[3];
			
			
		}
		
		String[] resStrings = new String[4];
		
		resStrings[0] = printCode;
		resStrings[1] = pandasCode;
		resStrings[2] = res;
		resStrings[3] = row_name;
		
		
		
		return resStrings;
	}

	public String[] getAccuracyOutput(ValidationMetric metric, String topImports, String pandasCode) {
		
		String[] allStrings = new String[2];
		
		
		if(metric == ValidationMetric.BALANCED_ACCURACY) {
			//topImports += "balanced_accuracy_score";
			pandasCode += "\ncat('laBALANCED_ACCURACY : v=', byClass$'Balanced Accuracy','\n')\n";
		}
		
		else if(metric == ValidationMetric.RECALL) {
			//topImports += "recall_score";
			pandasCode += "\ncat('leRECALL : v=', byClass$Recall,'\n')";
		}
		
		else if(metric == ValidationMetric.MACRO_RECALL) {
			//topImports += "recall_score";
			pandasCode += "\ncat('leMACRO_RECALL : v=', mean(byClass$Recall),'\n')\n";
		}
		
		else if(metric == ValidationMetric.PRECISION) {
			//topImports += "precision_score";
			pandasCode += "\ncat('laPRECISION : v=', byClass$Precision,'\n')\n";
		}
		
		else if(metric == ValidationMetric.MACRO_PRECISION) {
			//topImports += "precision_score";
			pandasCode += "\ncat('laMACRO_PRECISION : v=', mean(byClass$Precision),'\n')\n";
		}
		
		else if(metric == ValidationMetric.F1 ) {
			//topImports += "f1_score";
			pandasCode += "\ncat('leF1 : v=', byClass$F1,'\n')\n";
		}
		else if(metric == ValidationMetric.MACRO_F1) {
			//topImports += "f1_score";
			pandasCode += "\ncat('leMACRO_F1 : v=', mean(byClass$F1), '\n')\n";
		}
		
		else if(metric == ValidationMetric.ACCURACY) {
			//topImports += "accuracy_score";
			pandasCode += "\ncat('leACCURACY : v=', overall['Accuracy',], '\n')\n";
		}
		
		/*else if(metric == ValidationMetric.MACRO_ACCURACY) {
			topImports += "accuracy_score";
			pandasCode += "\nprint('MACRO_ACCURACY : ', accuracy_score(y_test, y_pred, average='macro'))";
		}*/
		
		allStrings[0] = topImports;
		allStrings[1] = pandasCode;
		
		return allStrings;
	}
	

	public String generateValidationMetricOutput(List<ValidationMetric> metrics, String pandasCode) {
		
		//Importation in the top of python
		String topImports = "from sklearn.metrics import ";
		
		
		
		ValidationMetric firstMetric = metrics.get(0);
		
		String[] allStrings = new String[2];
		
		allStrings = getAccuracyOutput(firstMetric, topImports, pandasCode);
		topImports = allStrings[0];
		pandasCode = allStrings[1];
		
		System.out.println(metrics.size());
		for(int i=1; i<metrics.size();i++) {
			
			topImports += ", "; //Ajout d'un nouveau dans l'import
			
			ValidationMetric metric = metrics.get(i);
			String[] _allStrings = new String[2];
			
			_allStrings = getAccuracyOutput(metric, topImports, pandasCode);
			
			topImports = _allStrings[0];
			pandasCode = _allStrings[1];
			
		}
		
		//Ajout des imports en haut du code
		//pandasCode = topImports + '\n' + pandasCode;
		
		
		return pandasCode;
	}
	
	
	public String generateModel(String algo, StratificationMethod stratMethod, List<ValidationMetric> metrics, String topCode, String modelCode) {
		
		if (stratMethod instanceof CrossValidation) {
			System.out.println("Cross validation method choosed");
			
			int kFold = stratMethod.getNumber();
			
			// Gestion des imports
			//pandasCode = "from sklearn.model_selection import KFold\n" + pandasCode;
			
			
			//Set X and y
			String pandasCode = "indexes <- sample(nrow(mml_data), (nrow(mml_data)*0.7))\n";
			
			//pandasCode += "\nX_train, X_test, y_train, y_test = train_test_split(X, y, test_size="+test_size+"/100, random_state=42)\n";
			pandasCode += "X_train <- mml_data[indexes,1:(ncol(mml_data)-1)]\n";
			pandasCode += "y_train <- mml_data[indexes,-(1:(ncol(mml_data)-1))]\n";
			pandasCode += "y_train_num<-as.numeric(y_train)\n";
			pandasCode += "train <-cbind(X_train, y=as.factor(y_train))\n";
			
			pandasCode += "X_test <- mml_data[-indexes,1:(ncol(mml_data)-1)]\n";
			pandasCode += "y_test <- mml_data[-indexes,-(1:(ncol(mml_data)-1))]\n";
			pandasCode += "y_test_num<-as.numeric(y_train)\n";
			pandasCode += "test <- cbind(X_test, y = as.factor(y_test))\n";
			
			pandasCode += "folds = createFolds(train$y, k = " + kFold + ")\n";
			
			topCode = topCode + pandasCode;
			
			topCode += "cv = lapply(folds, function(x) {\n";
			topCode += "\ttraining = train[-x, ]\n";
			topCode += "\ttest = train[x, ]\n";
			topCode += "\t"+ modelCode;
			topCode += "\ty_test = test[,-(1:(ncol(mml_data)-1))]\n";
			
			//topCode += "\ty_pred = predict(" + algo + ", newdata = test[,1:(ncol(mml_data)-1)],type=\"class\")\n";
			if (algo == "lr") {
				topCode +=  "\ty_pred <- predict(lr,newx = as.matrix(test[,1:(ncol(mml_data)-1)]),type= \"class\")\n";
				topCode += "\tCM <-confusionMatrix(y_test,as.factor(y_pred))\n";
			}else {
				topCode +=  "\ty_pred = predict(" + algo + ", newdata = test[,1:(ncol(mml_data)-1)],type=\"class\")\n";
				topCode += "\tCM <-confusionMatrix(y_test, y_pred)\n";
			}
			
			topCode += "\tbyClass<-as.data.frame(CM$byClass)\n";
			topCode += "\toverall<-as.data.frame(CM$overall)\n";
			
			
			topCode = generateValidationMetricOutput_cv(metrics, topCode)[1];
			String printCode = generateValidationMetricOutput_cv(metrics, topCode)[0];
			String res = generateValidationMetricOutput_cv(metrics, topCode)[2];
			String row_name = generateValidationMetricOutput_cv(metrics, topCode)[3];
			topCode += "\treturn(c(" + res + "))\n";
			topCode += "})\n";
			topCode += "df = as.data.frame(cv)\n"; 
			topCode += "row.names(df) = c(" + row_name + ")\n"; 
			topCode += "row_mean = apply(df,1,mean)\n";  
			topCode += printCode;
			

		}
		else if(stratMethod instanceof TrainingTest){
			
			TrainingTest tt = (TrainingTest) stratMethod;
			int test_size = tt.getNumber();
			
			//System.out.println(tt.getNumber());
			//System.out.println(test_size);
			
			// Gestion des imports
			//pandasCode = "from sklearn.model_selection import train_test_split\n" + pandasCode;
			
			
			
			//Set X and y
			String pandasCode = "indexes <- sample(nrow(mml_data), (nrow(mml_data)*(" + test_size + "/100)))\n";
			
			//pandasCode += "\nX_train, X_test, y_train, y_test = train_test_split(X, y, test_size="+test_size+"/100, random_state=42)\n";
			pandasCode += "X_train <- mml_data[indexes,1:(ncol(mml_data)-1)]\n";
			pandasCode += "y_train <- mml_data[indexes,-(1:(ncol(mml_data)-1))]\n";
			pandasCode += "y_train_num<-as.numeric(y_train)\n";
			pandasCode += "train <-cbind(X_train, y=as.factor(y_train))\n";
			
			pandasCode += "X_test <- mml_data[-indexes,1:(ncol(mml_data)-1)]\n";
			pandasCode += "y_test <- mml_data[-indexes,-(1:(ncol(mml_data)-1))]\n";
			pandasCode += "y_test_num<-as.numeric(y_train)\n";
			pandasCode += "test <- cbind(X_test, y = as.factor(y_test))\n";
			
			topCode = topCode + pandasCode;
					
			topCode += modelCode;
			if (algo == "lr") {
				topCode +=  "y_pred <- predict(lr,as.matrix(X_test),type= \"class\")\n";
				topCode += "CM <-confusionMatrix(y_test,as.factor(y_pred))\n";
			}else {
				topCode +=  "y_pred <- predict("+ algo + ",X_test,type= \"class\")\n";
				topCode += "CM <-confusionMatrix(y_test, y_pred)\n";
			}
			
			topCode += "byClass<-as.data.frame(CM$byClass)\n";
			topCode += "overall<-as.data.frame(CM$overall)\n";
			
			topCode = generateValidationMetricOutput(metrics, topCode);
			
		}
		
		
		return topCode + "\n\n" + "cat(paste(replicate(20, \"-\"), collapse = \"\"), \"\\n\")" + "\n\n";
	}
	
	

	
	public String getSVMOutput(String start, String C, String gamma, SVMKernel kernel, String pandasCode) {
		
		//Ajout du début de la méthode
		//pandasCode += start;
		
		if(kernel == SVMKernel.LINEAR) {
			if(C == null || (int) Integer.parseInt(C)<0) {
				pandasCode+="clf = svm(y ~ ., data = train, kernel = '" + kernel + "', cost = 1, scale = FALSE, type=" + start + ")\n";
				
			}
			else {
				pandasCode+="clf = svm(y ~ ., data = train, kernel = '" + kernel + "', cost = " + C + ", scale = FALSE, type=" + start + ")\n";	

			}
		}
		else if(kernel ==SVMKernel.POLY){
			if(C == null || (int) Integer.parseInt(C)<0) {
				if(gamma == null) {
					pandasCode+="clf = svm(y ~ ., data = train, kernel = 'polynomial', cost = 1, scale = FALSE, type=" + start + ")\n";
				}
				else {
					pandasCode+="clf = svm(y ~ ., data = train, kernel = 'polynomial', cost = 1, scale = FALSE, gamma = " + gamma + ",type=" + start + ")\n";
				}
			}
			else {
				if(gamma == null) {
					pandasCode+="clf = svm(y ~ ., data = train, kernel = 'polynomial', cost = " + C + ", scale = FALSE, type=" + start + ")\n";
				}
				else {
					pandasCode+="clf = svm(y ~ ., data = train, kernel = 'polynomial', cost = " + C + ", scale = FALSE, gamma = " + gamma + ",type=" + start + ")\n";

				}
			}
		}
		else if(kernel ==SVMKernel.RADIAL){//radial basis
			if(C == null || (int) Integer.parseInt(C)<0) {
				if(gamma == null) {
					pandasCode+="clf = svm(y ~ ., data = train, kernel = 'radial', cost = 1, scale = FALSE, type=" + start + ")\n";
				}
				else {
					pandasCode+="clf = svm(y ~ ., data = train, kernel = 'radial', cost = 1, scale = FALSE, gamma = " + gamma + ",type=" + start + ")\n";
				}
			}
			else {
				if(gamma == null) {
					pandasCode+="clf = svm(y ~ ., data = train, kernel = 'radial', cost = " + C + ", scale = FALSE, type=" + start + ")\n";
				}
				else {
					pandasCode+="clf = svm(y ~ ., data = train, kernel = 'radial', cost = " + C + ", scale = FALSE, gamma = " + gamma + ",type=" + start + ")\n";

				}
			}
		}
		return pandasCode;
	}
}
