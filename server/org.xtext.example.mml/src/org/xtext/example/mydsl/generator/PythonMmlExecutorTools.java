package org.xtext.example.mydsl.generator;

import java.util.List;

import org.xtext.example.mydsl.mml.CrossValidation;
import org.xtext.example.mydsl.mml.SVMKernel;
import org.xtext.example.mydsl.mml.StratificationMethod;
import org.xtext.example.mydsl.mml.TrainingTest;
import org.xtext.example.mydsl.mml.ValidationMetric;

public class PythonMmlExecutorTools {
	
	public String[] getAccuracyOutput(ValidationMetric metric, String topImports, String pandasCode) {
		
		String[] allStrings = new String[2];
		
		if(metric == ValidationMetric.BALANCED_ACCURACY) {
			topImports += "balanced_accuracy_score";
			pandasCode += "\nprint('BALANCED_ACCURACY : ', balanced_accuracy_score(y_test, y_pred))";
		}
		
		else if(metric == ValidationMetric.RECALL) {
			topImports += "recall_score";
			pandasCode += "\nprint('RECALL :', recall_score(y_test, y_pred, average=None))";
		}
		
		else if(metric == ValidationMetric.MACRO_RECALL) {
			topImports += "recall_score";
			pandasCode += "\nprint('MACRO_RECALL :', recall_score(y_test, y_pred, average='macro'))";
		}
		
		else if(metric == ValidationMetric.PRECISION) {
			topImports += "precision_score";
			pandasCode += "\nprint('PRECISION : ', precision_score(y_test, y_pred, average=None))";
		}
		
		else if(metric == ValidationMetric.MACRO_PRECISION) {
			topImports += "precision_score";
			pandasCode += "\nprint('MACRO_PRECISION : ', precision_score(y_test, y_pred, average='macro'))";
		}
		
		else if(metric == ValidationMetric.F1 ) {
			topImports += "f1_score";
			pandasCode += "\nprint('F1 : ', f1_score(y_test, y_pred, average=None))";
		}
		else if(metric == ValidationMetric.MACRO_F1) {
			topImports += "f1_score";
			pandasCode += "\nprint('MACRO_F1 : ', f1_score(y_test, y_pred, average='macro'))";
		}
		
		else if(metric == ValidationMetric.ACCURACY) {
			topImports += "accuracy_score";
			pandasCode += "\nprint('ACCURACY : ', accuracy_score(y_test, y_pred))";
		}
		
		else if(metric == ValidationMetric.MACRO_ACCURACY) {
			topImports += "accuracy_score";
			pandasCode += "\nprint('MACRO_ACCURACY : ', accuracy_score(y_test, y_pred))";
		}
		
		allStrings[0] = topImports;
		allStrings[1] = pandasCode;
		
		return allStrings;
	}
	

	public String generateValidationMetricOutput(List<ValidationMetric> metrics, String pandasCode) {
		
		//Importation in the top of python
		String topImports = "from sklearn.metrics import ";
		
		ValidationMetric firstMetric = metrics.get(0);
		
		String[] allStrings = new String[2];
		allStrings = getAccuracyOutput(firstMetric, topImports, pandasCode);
		topImports = allStrings[0];
		pandasCode = allStrings[1];
		
		//System.out.println(metrics.size());
		for(int i=1; i<metrics.size();i++) {
			
			topImports += ", "; //Ajout d'un nouveau dans l'import
			
			ValidationMetric metric = metrics.get(i);
			String[] _allStrings = new String[2];
			
			_allStrings = getAccuracyOutput(metric, topImports, pandasCode);
			
			topImports = _allStrings[0];
			pandasCode = _allStrings[1];
			
		}
		
		//Ajout des imports en haut du code
		pandasCode = topImports + '\n' + pandasCode;
		
		
		return pandasCode;
	}
	
	
	public String generateModel(String algo, StratificationMethod stratMethod, List<ValidationMetric> metrics, String pandasCode) {
		
		if (stratMethod instanceof CrossValidation) {
			System.out.println("Cross validation method choosed");
			
			int kFold = stratMethod.getNumber();
			
			// Gestion des imports
			pandasCode = "from sklearn.model_selection import KFold\n" + pandasCode;
			
			//Set X and y
			pandasCode+= "X, y = mml_data.iloc[:,0:len(mml_data.columns)-1], mml_data.iloc[:,len(mml_data.columns)-1]";
			
			pandasCode += "kf = KFold(n_splits = "+ kFold + ")";
			pandasCode += "for train_index, test_index in kf.split(X):\n\t";
			pandasCode += "X_train, X_test = X[train_index], X[test_index]\n\t";
			pandasCode +=  "y_train, y_test = y[train_index], y[test_index]\n\t";
			
			//fit predict
			pandasCode +=  algo + ".fit(X_train, y_train)\n\t";
			pandasCode +=  "y_test = " + algo + ".predict(X_test)\n\t";
			
			//Importation des packages necessaires et print des validations scores
			pandasCode = generateValidationMetricOutput(metrics, pandasCode);
			
		}
		else if(stratMethod instanceof TrainingTest){
			
			TrainingTest tt = (TrainingTest) stratMethod;
			int test_size = 100 - tt.getNumber();
			
			//System.out.println(tt.getNumber());
			//System.out.println(test_size);
			
			// Gestion des imports
			pandasCode = "from sklearn.model_selection import train_test_split\n" + pandasCode;
			
			//Set X and y
			pandasCode+= "\nX, y = mml_data.iloc[:,0:len(mml_data.columns)-1], mml_data.iloc[:,len(mml_data.columns)-1]";
			
			pandasCode += "\nX_train, X_test, y_train, y_test = train_test_split(X, y, test_size="+test_size+"./100)\n";
			
			//fit predict
			pandasCode +=  algo + ".fit(X_train, y_train)\n";
			pandasCode +=  "y_pred = " + algo + ".predict(X_test)";
			
			pandasCode = generateValidationMetricOutput(metrics, pandasCode);
			
		}
		
		
		return pandasCode + "\n\n" + "print('-'*20)" + "\n\n";
	}
	
	
	public String getSVMOutput(String start, String C, String gamma, SVMKernel kernel, String pandasCode) {
		
		if(kernel == SVMKernel.LINEAR) {
			if(C == null || (int) Integer.parseInt(C)<0) {
				if(gamma == null) {
					pandasCode+="clf = " + start + "(C=1.0, gamma='scale', kernel='" + "linear" + "')";
				}
				else {
					pandasCode+="clf = " + start + "(C=1.0, gamma='" + gamma + "', kernel='" + "linear" + "')";
				}
			}
			else {
				if(gamma == null) {
					pandasCode+="clf = " + start + "(C=" + C + ", gamma='scale', kernel='" + "linear" + "')";
				}
				else {
					pandasCode+="clf = " + start + "(C=" + C + ", gamma='" + gamma + "', kernel='" + "linear" + "')";
				}
			}
		}
		else if(kernel ==SVMKernel.POLY){
			if(C == null || (int) Integer.parseInt(C)<0) {
				if(gamma == null) {
					pandasCode+="clf = " + start + "(C=1.0, gamma='scale', kernel='" + "poly" + "')";
				}
				else {
					pandasCode+="clf = " + start + "(C=1.0, gamma='" + gamma + "', kernel='" + "poly" + "')";
				}
			}
			else {
				if(gamma == null) {
					pandasCode+="clf = " + start + "(C=" + C + ", gamma='scale', kernel='" + "poly" + "')";
				}
				else {
					pandasCode+="clf = " + start + "(C=" + C + ", gamma='" + gamma + "', kernel='" + "poly" + "')";
				}
			}
		}
		else if(kernel ==SVMKernel.RADIAL){
			if(C == null || (int) Integer.parseInt(C)<0) {
				if(gamma == null) {
					pandasCode+="clf = " + start + "(C=1.0, gamma='scale', kernel='" + "rbf" + "')";
				}
				else {
					pandasCode+="clf = " + start + "(C=1.0, gamma='" + gamma + "', kernel='" + "rbf" + "')";
				}
			}
			else {
				if(gamma == null) {
					pandasCode+="clf = " + start + "(C=" + C + ", gamma='scale', kernel='" + "rbf" + "')";
				}
				else {
					pandasCode+="clf = " + start + "(C=" + C + ", gamma='" + gamma + "', kernel='" + "rbf" + "')";
				}
			}
		}
		return pandasCode;
	}
}
