from sklearn.metrics import recall_score, precision_score, f1_score, recall_score
from sklearn.model_selection import train_test_split
from sklearn import svm
import pandas as pd
mml_data = pd.read_csv('iris.csv', sep=',')
print(mml_data.head())

clf = svm.SVC(C = 1)
X, y = mml_data.iloc[:,0:len(mml_data.columns)-1], mml_data.iloc[:,len(mml_data.columns)-1]
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=30/100)
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)

print('RECALL :', recall_score(y_test, y_pred, average=None))
print('PRECISION : ', precision_score(y_test, y_pred, average=None))
print('F1 : ', f1_score(y_test, y_pred, average=None))
print('MACRO_RECALL :', recall_score(y_test, y_pred, average='macro'))