# MML
MML is a Machine Learning library for classification and prediction tasks. Through this library, we make some of the most famous Machine Learning algorithms easier to wield by common users.\
 In its current form, MML supports 4 main Machine Learning algorithms namely: 
- SVM
- Decision tree 
- Random forest
- Logistic regression

 It allows the users to choose the database they want to perform their analysis on provided it's in the CSV form. If no dataset is selected by the user, MML chooses the famous IRIS dataset by default. \
In addition to running Machine Learning algorithms on datasets, MML proposes a wide variety of metrics to evaluate the performance of these models. \
These metrics include, but are not limited to:
- F1 score
- F1-scoremicro, 
- Recall
- Balanced Accuracy
- Accuracy

An additional option available is to cross-validate the model’s results in order to get amore precise estimation of the model’s performance.\
This simple MML code will then be transformed behind the curtains and depending on the user's choice into one of these 3 Frameworks:
- R
- Scikit-Learn
- JAVA

The user could choose more than one Framework in order to compare between the computation time of different Frameworks.


