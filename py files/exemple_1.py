import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn import tree
from sklearn.metrics import accuracy_score
from sklearn.neural_network import MLPClassifier


# Using pandas to import the dataset
df = pd.read_csv("monoprix.csv")


# Spliting dataset between features (X) and label (y)
X = df.drop(columns=df[,:3])
y = df[:,3]




# Set algorithm to use
clf = MLPClassifier()



# Use the algorithm to create a model with the training set
clf.fit(X, y)

# Compute and display the accuracy
k-fold=5
accuracy = cross_val_score(clf, monoprix.dat, k-fold)

print(accuracy)



